public class Hardware{
	private String name;
	private String category;
	private double price;
		
	public Hardware( String name, String category, double price ){
		this.name = name;
		this.category = category;
		this.price = price;		
	}
		
	public void displayAttributes(){
		System.out.println( name+"\t"+category+"\t"+price );
	}
}