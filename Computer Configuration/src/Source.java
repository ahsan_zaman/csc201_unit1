/*
 * Ahsan Zaman
 * CSC 201 - Unit 1
 * Computer COnfiguration
 * 
 * Algorithm:
 * 1. Declare and Initialize Hardware objects
 * 2. Display Hardware objects in a table
 * 3. Display upgrades and ask user for the marketprices
 * 4. Display the upgrades in a table
 */
import java.util.Scanner;
public class Source {
	
	public static void main( String[] args ){
		Scanner input = new Scanner( System.in );
		System.out.println( "\t\t**Computer Hardware Upgrade**" );
		Hardware[] computerParts = new Hardware[5];
		double priceEntered=0.0;
		// Giving the initial values to the objects
		computerParts[0] = new Hardware( "Rosewell Minitower", "Case\t\t", 29 );
		computerParts[1] = new Hardware( "Gigabyte Z797\t", "MotherBoard\t", 100 );
		computerParts[2] = new Hardware( "Stock\t\t", "Cooling Unit\t", 0 );
		computerParts[3] = new Hardware( "Rosewill Capstone 750W", "Power Supply\t", 40 );
		computerParts[4] = new Hardware( "Intel Core i3\t", "Processor\t", 100 );
		
		System.out.println( "Component Name\t\tCategory\t\tPrice");
		for( int i=0; i<5 ;i++ ){
			computerParts[i].displayAttributes();
		}
		
		System.out.println( "\nThis build is pretty good but"
				+ "you can probably do with an upgrade. \nYou"
				+ " can enter the prices of the compononents yourself.\n " );
		// Asking user for the prices of the upgraded parts
		System.out.println( "Enter price for Antec ATX Midtower: " );
		priceEntered = input.nextDouble();
		computerParts[0] = new Hardware( "Antec ATX\t", "Case\t\t", priceEntered );
		System.out.println( "Enter the price for a MSI Z87 Motherboard: " );
		priceEntered = input.nextDouble();
		computerParts[1] = new Hardware( "MSI Z87\t\t", "Motherboard\t", priceEntered );
		System.out.println( "Enter the price for Seidon 240M: " );
		priceEntered = input.nextDouble();
		computerParts[2] = new Hardware( "Seidon 240M\t", "Cooling Unit\t", priceEntered );
		System.out.println( "Enter the price for Corsair HX850 power supply: " );
		priceEntered = input.nextDouble();
		computerParts[3] = new Hardware( "Corsair HX850\t", "Power Supply\t", priceEntered );
		System.out.println( "Enter price for an Intel i7 processor: " );
		priceEntered = input.nextDouble();
		computerParts[4] = new Hardware( "Intel Core i7\t", "Processor\t", priceEntered );
		
		// Displaying results in a table
		System.out.println( "Component Name\t\tCategory\t\tPrice");
		for( int i=0; i<5 ;i++ ){
			computerParts[i].displayAttributes();
		}
		input.close();
	}
}
