/* Ahsan Zaman
 * CSC 201 - Vending Machine
 * 
 */

import java.util.Scanner;
public class Source {
	public static double money=0.0;					// Total sum of money entered into machine
	
	public static void main( String[] args ){		// Main menu
		Scanner input = new Scanner(System.in);
		int choice=6;
		while( choice!=0 ){
			System.out.println( "**Vending Machine**\nChoose one of the following options or press 0 to exit at any time: " );
			System.out.println( "1. Enter money into machine\n2. Make a selection" );
			choice = input.nextInt();
			if( choice==0 )
				giveChange();
			
			else if( choice==1 )
				enterMoney();
			
			else if( choice==2 )
				selectionMenu();
			
			else System.out.println( "Wrong choice entered. Try again." );
		}

		input.close();
	}
	
	public static void enterMoney(){
		Scanner input = new Scanner(System.in);
		double entry=0.0;							// Money that will be entered in coins, 1$ or 5$ bill
		System.out.println( "\n**Vending Machine**\nProducts: \n   Product\t\tPrice " );
		System.out.println( "1. Lays Chips\t\t1.50$ \n2. Cheese Pops\t\t1.30$\n3. Scantron\t\t1.25$ \n4. Kitkat\t\t1.15$\n5. Munchies\t\t1.00$" );
		System.out.println( "Enter money in coins, 1$ bill or 5$ bill (one at a time) to purchase a product and then press -1: " );
		entry = input.nextDouble();
		
		if( entry==1 || entry==0.25 || entry==0.10 || entry==0.01 || entry==0.05 || entry==5 ){
			money+=entry;							// Ensuring money is entered one at a time and in correct form
		}
		money = (double) Math.round(money*100)/100;
		System.out.println( "Money: "+money );		// Displaying the total sum entered into the machine
		
	}
	
	public static void selectionMenu(){				// User chooses product here or presses 0 to exit
		Scanner input = new Scanner( System.in );
		int choice=6;
		System.out.println( "\n**Vending Machine**\nPlease choose one of these following products or enter 0 to exit: \n   Product\t\tPrice " );
		System.out.println( "1. Lays Chips\t\t1.50$ \n2. Cheese Pops\t\t1.30$\n3. Scantron\t\t1.25$ \n4. Kitkat\t\t1.15$\n5. Munchies\t\t1.00$" );
		choice = input.nextInt();
		
		switch( choice ){
		case 0:
			giveChange();							// Cancelling transaction
			break;
		case 1:
			if( money>=1.5 ){						// Deducting money and giving change
				money-=1.5;
				money = (double) Math.round(money*100)/100;
				System.out.println( "Enjoy! \nYour change is "+money);
				giveChange();
			}
			else{
				
			}
			break;
		case 2:
			if( money>=1.3 ){
				money-=1.3;
				money = (double) Math.round(money*100)/100;
				System.out.println( "Enjoy! \nYour change is "+money );
				giveChange();
			}
			else{
				
			}
			break;
		case 3:
			if( money>=1.25 ){
				money-=1.25;
				money = (double) Math.round(money*100)/100;
				System.out.println( "Enjoy! \nYour Change is "+money );
				giveChange();
			}
			else{
				
			}
			break;
		case 4: 
			if( money>=1.15 ){
				money-=1.15;
				money = (double) Math.round(money*100)/100;
				System.out.println( "Enjoy! \nYour change is "+money );
				giveChange();
			}
			break;
		case 5:
			if( money>=1.00 ){
				money-=1;
				money = (double) Math.round(money*100)/100;
				System.out.println( "Enjoy! \nYour change is "+money );
				giveChange();
			}
			break;
			default:
				System.out.println( "Incorrect option chosen. " );
		}
	}
	
	public static void giveChange(){				// Exit sequence; User is given their change first
		int quarters=0, dimes=0, nickels=0, pennies=0;
		
		while( money != 0 ){							// Loop will run numerous times until it has all the change ready
			if( money>=0.25 ){
				money-=0.25;
				
				quarters++;
			}
			else if( money>=0.10 ){
				money-=0.10;
				dimes++;
			}
			else if( money>=0.05 ){
				money-=0.05;
				nickels++;
			}
			else if( money>=0.01 ){
				money-=0.01;
				pennies++;
			}
			else money=0;
			money = (double) Math.round(money*100)/100;
		}
		System.out.println( "\n*******************************");
		System.out.println( "\tReceipt\n*******************************" );
		System.out.println( "Your change:\nQuarters: "+quarters+"\nDimes: "+dimes+"\nNickels: "+nickels+"\nPennies: "+pennies );
		System.out.println( "Total change returned: "+( ( quarters*0.25 )+( dimes*0.1 )+( nickels*0.05 )+( pennies*0.01 ) ) );
		System.out.println( "*******************************" );
	}
	
}
