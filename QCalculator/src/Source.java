import java.util.Scanner;
public class Source {
	public static void main( String[] args ){
		
		Scanner input = new Scanner(System.in);	// For input
		int SID=0, divisor=0, increment=0, chapter=0, question=0;	//All variables
		
		System.out.println("**Program to Calculate the Question for Computer Science 1**\n ");

		while( SID != -1 ){		// Loop  ends when user wants.
			System.out.println("Enter the student's ID number or press -1 to exit: ");
			SID = input.nextInt();	
			
			System.out.println( "Enter the following values to get the Chapter Number:\nDivisor: " );
			divisor = input.nextInt();
			chapter = SID%divisor;
			System.out.println( "Increment: " );
			increment = input.nextInt();
			chapter += increment;	// Storing the final value
			
			System.out.println( "Chapter Number: "+chapter+"\nQuestion: "+question );
			
			System.out.println( "Enter the following values to get the question number:\nDivisor: " );
			divisor = input.nextInt();
			question = SID%divisor;
			System.out.println("Increment: ");
			increment = input.nextInt();
			question += increment;	// Storing the final value
			
			System.out.println( "Chapter Number: "+chapter+"\nQuestion: "+question );	// Displaying results
		}
		input.close();
	}
}
