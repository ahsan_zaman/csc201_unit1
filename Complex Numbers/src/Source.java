/*	
 * 	Ahsan Zaman
 * 	CSC 201 - Unit 1
 *  Complex Numbers
 *  
 *  Algorithm:
 *  1. Declare and Initialize the three complexNumber objects
 *  2. Declare and Initialize integer variables choice, real, complex
 *  3. Ask user to input the real and complex values for the two complex numbers
 *  4. Ask user which operation he/she wants to perform and store their choice entered in variable choice 
 *  5. Use switch case to match choice to the appropriate action
 *  6. Use the appropriate formula to perform the desired operation
 *  7. Display the resulting complexNumber
 *  
 * */

import java.util.Scanner;
public class Source {
	public static void main( String[] args ){	
		Scanner input = new Scanner(System.in);
		complexNumber z1, z2, z3;				// Declaring three complex Numbers
		int choice=0, real=0, complex=0;
												// Entering first complex number
		System.out.print( "Enter first complex Number, z1's values: \nReal: " );
		real = input.nextInt();
		System.out.print( "Complex : " );
		complex = input.nextInt();
		z1 = new complexNumber( real, complex );
												// Entering second complex number
		System.out.print( "Enter second complex number, z2's values: \nReal: ");
		real = input.nextInt();
		System.out.print( "Complex: " );
		complex = input.nextInt();
		z2 = new complexNumber( real, complex );
		
		while( choice!= 5 ){
												// Displaying both complex Numbers and menu
			System.out.print( "\n\nz1 = " );
			z1.displayNumber();
			System.out.print( "\nz2 = " );
			z2.displayNumber();
			
			System.out.println( "\nPlease choose which operation to perfrom or press 5 to exit: " );
			System.out.println( "1. Addition z1+z2\n2. Subtraction z1-z2" );
			System.out.println( "3. Multiplication z1*z2\n4. Division z1/z2" );
			
			choice = input.nextInt();
			switch( choice ){					// All processes are done through complexNumber class methods
			case 1:
				z3 = z1.addition(z2);			
				System.out.print( "\nAnswer: " );
				z3.displayNumber();
				break;
			case 2:
				z3 = z1.subtraction(z2);
				System.out.print( "\nAnswer: " );
				z3.displayNumber();
				break;
			case 3:
				z3 = z1.multiplication(z2);
				System.out.print( "\nAnswer: " );
				z3.displayNumber();
				break;
			case 4:
				z3 = z1.division(z2);
				System.out.print( "\nAnswer: " );
				z3.displayNumber();
				break;
			case 5:
				System.out.println( "Exit Sequence Initiated. " );
				break;
				default:
					System.out.println( "Incorrect Entry. " );
			}
		}
		input.close();
	}
}
