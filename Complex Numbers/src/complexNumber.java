/*
 * Ahsan Zaman
 * CSC 201 - Unit 1
 * Complex Numbers
 * 
 * complexNumber class
 */
public class complexNumber {
	private int a;			// Real number
	private int b;			// Complex Number
	
	public complexNumber( int real, int complex ){
		a = real;
		b = complex;
	}
	
	public complexNumber addition( complexNumber z2 ){
		complexNumber temp;
		temp = new complexNumber( ( a+z2.a ), ( b+z2.b ) );
		return temp;		// Returning the sum in an object
	}
	
	public complexNumber subtraction( complexNumber z2 ){
		complexNumber temp;
		temp = new complexNumber( ( a-z2.a ), ( b-z2.b ) );
		return temp;		// Returning the difference in an object
	}
	
	public complexNumber multiplication( complexNumber z2 ){
		complexNumber temp;	// Computing using the formula given in the question
		int real=0, complex=0;
		real = ( a*z2.a )-( b*z2.b );
		complex = ( a*z2.b )+( b*z2.a );
		temp = new complexNumber( real, complex );
		return temp;
	}
	
	public complexNumber division( complexNumber z2 ){
		complexNumber temp;	// Computing using the formula given in the question
		int real=0, complex=0;
		real = ( a*z2.a + b*z2.b )/( ( z2.a*z2.a ) + ( z2.b*z2.b ) );
		complex = ( b*z2.a - a*z2.b )/( ( z2.a*z2.a ) + ( z2.b*z2.b ) );
		temp = new complexNumber( real, complex );
		return temp;
	}
	
	public void displayNumber(){
		System.out.print( a+" + ( "+b+" )i" );
	}
}
