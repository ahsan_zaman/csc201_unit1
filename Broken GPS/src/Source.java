/*
 * Ahsan Zaman
 * CSC 201 - Unit 1
 * Broken GPS Program
 * 
 * Algorithm:
 * 1. Declare and Initialize xCoord and yCoord integer variables
 * 2. Decalare and initialize final int variable TURNS to 12
 * 3. Display initial coordinates
 * 4. Start the loop which will run 12 times
 * 5. In the loop call newDirection to generate a new direction for each intersection
 * 6. Call calculateDistance() to find out the distance
 * 7. Display the distance with the final coordinates 
 */

import java.util.Random;

public class Source {
	private static int xCoord=0;
	private static int yCoord=0;
	
	public static void main( String[] args ){
		final int TURNS=12;
		
		System.out.println( "\t\t**Broken GPS**\nSpeed: 60 miles/hour \nDriver starting point: ( "+xCoord+", "+yCoord+" )" );
		System.out.println( "Random Directions took at each intersection: " );
		
		for( int i=0; i<TURNS ;i++ ){
			System.out.println( newDirection() );			// Determines a random direction
			System.out.println( "New Coordinates: "+"( "+xCoord+", "+yCoord+" )" );
		}
		
		System.out.println( "\n->Final Coordinates: "+"( "+xCoord+", "+yCoord+" )" );
		System.out.println( "Distance: "+calculateDistance() +" miles" );
	}
	
	public static char newDirection(){
		
		char direction='N';
		int dir;											// dir will store a random value between 0 and 3 as a supposed direction
		Random randGen = new Random();
		dir = randGen.nextInt(4);							// gives a random number from 0 to 3
															// from 0  to 3 each is taken as a direction
		if( dir==0){										// North
			direction = 'N';
			yCoord++;
		}
		else if( dir==1 ){									// South
			direction = 'S';
			yCoord--;
		}
		else if( dir==2 ){									// East
			xCoord--;
			direction = 'E';
		}
		else if( dir==3){									// West
			xCoord++;
			direction = 'W';
		}
		return direction;									// Returns this new direction
	}
	
	public static double calculateDistance(){
		double distance=0;
															// formula for distance = sqrt( (x2-x1)^2 + (y2-y1)^2 ); as x1 and y1 are 0 they are not needed
		distance = Math.pow( xCoord, 2 )+ Math.pow( yCoord, 2 );	
		if( distance!=0 & distance!=1 )						// Do not need to compute these values
			distance = Math.sqrt( distance );
		distance*=5;										// As each intersection is 5 miles away 
		distance = Math.round(distance*100)/100;			// Rounding off the answer to 2 decimal places
		return distance;
	}
}
